package homework_5;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import static org.testng.Assert.assertEquals;

public class GeneralSetup {
    protected WebDriver driver;
    protected WebDriverWait wait;

    @BeforeMethod
    public void openMailbox () {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 60);
        driver.get("https://mail.yandex.ru");
        driver.manage().window().maximize();

        //      проверили, что зашли на нужный сайт
        String title = driver.getTitle();
        assertEquals(title, "Яндекс.Почта — бесплатная и надежная электронная почта");
//      или
//      String a = "Яндекс";
//      Assert.assertTrue(title.contains(a));


//      Войти в почту
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Войти"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("passp-field-login"))).sendKeys("FeElena2020");

        wait.until(ExpectedConditions.elementToBeClickable(By.className("passp-sign-in-button"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.name("passwd"))).sendKeys("Brusnika2020");
        wait.until(ExpectedConditions.elementToBeClickable(By.className("passp-sign-in-button"))).click();

//        Assert, что вход выполнен успешно
        WebElement userIcon = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("user-account__name")));
        AssertJUnit.assertEquals(userIcon.getText(), "FeElena2020");
      }


    public void catchStaleExceptions  (WebElement element, ExpectedCondition condition) {
        try {
            wait.until(condition);
            element.click();
        } catch(StaleElementReferenceException e) {
                wait.until(condition);
                element.click();
            }
        }

    @AfterMethod
    public void quitMailbox() {
        driver.quit();
    }
}
