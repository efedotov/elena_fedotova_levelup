package homework_5;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.List;
import static org.testng.Assert.assertTrue;

public class Exercise_3 extends GeneralSetup {

    @Test
    public void exercise3() {

        //   Создать новое письмо (заполнить адресата (самого себя), тему письма (должно содержать слово Тест) и тело)
        wait.until(ExpectedConditions.elementToBeClickable(By.className("svgicon-mail--ComposeButton"))).click();

        WebElement addressInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("composeYabbles")));
        Actions actions = new Actions(driver);
        actions
                .click(addressInput)
                .sendKeys("feelena2020@yandex.ru")
                .sendKeys(Keys.ENTER)
                .build()
                .perform();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("ComposeSubject-TextField"))).sendKeys("Еще одно письмо");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("cke_wysiwyg_div"))).sendKeys("qui dolorem ipsum, quia dolor sit");

        //          Отправить письмо
        wait.until(ExpectedConditions.elementToBeClickable(By.className("ComposeControlPanelButton-Button_action"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Вернуться во \"Входящие\""))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.className("svgicon-mail--ComposeButton-Refresh"))).click();

        //          Verify, что письмо появилось в папке Входящие
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")));
        List<WebElement> listOfEmailsIncoming = driver.findElements(By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']"));
        System.out.println(listOfEmailsIncoming.get(0).getText());
        assertTrue(listOfEmailsIncoming.get(0).getText().equals("Еще одно письмо"));;

        //   Verify контент, адресата и тему письма (должно совпадать с пунктом 3), зайти в письмо
        wait.until(ExpectedConditions.elementToBeClickable(By.className("mail-MessageSnippet-Item_body"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='mail-MessageSnippet-Wrapper']/a[@class='mail-MessageSnippet js-message-snippet toggles-svgicon-on-important toggles-svgicon-on-unread']/div[@class='mail-MessageSnippet-Content']/span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_left js-message-snippet-left']/span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender']"))).click();

        WebElement recipient = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("mail-Message-Sender-Email")));
        Assert.assertEquals(recipient.getText(), "feelena2020@yandex.ru");

        WebElement themeEmail = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("mail-Message-Toolbar-Subject_message")));
        Assert.assertEquals(themeEmail.getText(), "Еще одно письмо");

        WebElement emailBody = driver.findElement(By.className("mail-Message-Body-Content"));
        Assert.assertEquals(emailBody.getText(), "qui dolorem ipsum, quia dolor sit");

//        Удалить письмо
        WebElement delete = driver.findElement(By.className("js-toolbar-item-title-delete"));
        delete.click();

        wait.until(ExpectedConditions.elementToBeClickable(By.className("mail-MessageSnippet-Checkbox-Nb"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.className("js-toolbar-item-delete"))).click();

//        Verify что письмо появилось в папке Корзина
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@data-title='Удалённые']"))).click();

    }
}
