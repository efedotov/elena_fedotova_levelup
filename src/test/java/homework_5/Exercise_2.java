package homework_5;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.List;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;
import static org.testng.Assert.assertTrue;

public class Exercise_2 extends GeneralSetup {
    @Test
    public void exercise2() {

//        Создать новое письмо (заполнить адресата (самого себя), тему письма (должно содержать слово Тест) и тело)
        wait.until(elementToBeClickable(By.className("svgicon-mail--ComposeButton"))).click();

        WebElement addressInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("composeYabbles")));
        Actions actions = new Actions(driver);
        actions
                .click(addressInput)
                .sendKeys("feelena2020@yandex.ru")
                .sendKeys(Keys.ENTER)
                .build()
                .perform();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("ComposeSubject-TextField"))).sendKeys("Test");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("cke_wysiwyg_div"))).sendKeys("any text");

//          Отправить письмо
        wait.until(elementToBeClickable(By.className("ComposeControlPanelButton-Button_action"))).click();

//        Verify, что письмо появилось в папке отправленные
        wait.until(elementToBeClickable(By.linkText("Отправленные"))).click();
        actions
                .sendKeys(Keys.F9)
                .build()
                .perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")));
        List<WebElement> listOfEmailsSent = driver.findElements(By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']"));
        System.out.println(listOfEmailsSent.get(0).getText());
        assertTrue(listOfEmailsSent.get(0).getText().equals("Test"));

//        Verify, что письмо появилось в папке «Тест»
        wait.until(elementToBeClickable(By.className("svgicon-mail--ComposeButton-Refresh"))).click();
        wait.until(elementToBeClickable(By.xpath("//a[@data-title='Test']"))).click();

//        Verify контент, адресата и тему письма (должно совпадать с пунктом 3)
        List<WebElement> listOfEmailsTest = driver.findElements(By.xpath("//span[@title='Test']"));
        actions
                .click(listOfEmailsTest.get(0))
                .build()
                .perform();

        wait.until(numberOfElementsToBeMoreThan(By.xpath("//span[contains(@class,'js-message-snippet-sender')]"), 2));
        List<WebElement> listOfEmailsItemsTest = driver.findElements(By.xpath("//span[contains(@class,'js-message-snippet-sender')]"));
        catchStaleExceptions(listOfEmailsItemsTest.get(1), ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(@class,'js-message-snippet-sender')]")));

        WebElement recipient = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("mail-Message-Sender-Email")));
        Assert.assertEquals(recipient.getText(), "feelena2020@yandex.ru");

        WebElement themeEmail = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("mail-Message-Toolbar-Subject_message")));
        Assert.assertEquals(themeEmail.getText(), "Test");

        WebElement emailBody = driver.findElement(By.className("mail-Message-Body-Content"));
        Assert.assertEquals(emailBody.getText(), "any text");

        WebElement delete = driver.findElement(By.className("js-toolbar-item-title-delete"));
        delete.click();
//        wait.until(elementToBeClickable(By.className("mail-MessageSnippet-Checkbox-Nb"))).click();
//        wait.until(elementToBeClickable(By.className("js-toolbar-item-delete"))).click();
    }
}
