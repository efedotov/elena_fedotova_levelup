package homework_5;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.*;
import java.util.List;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.testng.Assert.*;

public class Exercise_1 extends GeneralSetup{

    @Test
    public void exercise1() {

//        Создать новое письмо (заполнить адресата, тему письма и тело)
        wait.until(elementToBeClickable(By.className("svgicon-mail--ComposeButton"))).click();
        WebElement addressInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("composeYabbles")));
        Actions actions = new Actions(driver);
        actions
                .click(addressInput)
                .sendKeys("feelena2020@yandex.ru")
                .sendKeys(Keys.ENTER)
                .build()
                .perform();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("ComposeSubject-TextField"))).sendKeys("Homework_5");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("cke_wysiwyg_div"))).sendKeys("Trying hard..");

//        Сохранить его как черновик
        wait.until(elementToBeClickable(By.className("ComposeControlPanelButton-Button_icon"))).click();
        wait.until(elementToBeClickable(By.className("ComposeTimeOptions-Label"))).click();

        WebElement closeEmail = driver.findElement(By.xpath("//div[@class='ComposePopup-Content']//button[contains(@class, 'controlButtons__btn--close')]"));
        closeEmail.click();

//        Verify, что письмо сохранено в черновиках
        wait.until(elementToBeClickable(By.linkText("Черновики"))).click();

//        вывод тем всех писем на консоль, выводит только по полному набору классов, по одному классу не находит
//        List<WebElement> listOfEmailsByTheme = driver.findElements(By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']"));
//        for (WebElement webElement : listOfEmailsByTheme) {
//        System.out.println(webElement.getText());
//        }
//        System.out.println();

        WebElement refreshButton = driver.findElement(By.xpath("//span[@title='Проверить, есть ли новые письма (F9)']"));
        catchStaleExceptions(refreshButton, ExpectedConditions.elementToBeClickable(By.xpath("//span[@title='Проверить, есть ли новые письма (F9)']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")));

        List<WebElement> listOfEmailsByTheme = driver.findElements(By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']"));
        System.out.println(listOfEmailsByTheme.get(0).getText());
        assertTrue(listOfEmailsByTheme.get(0).getText().equals("Homework_5"));

//        здесь пробовала сверить только по первому письму через индексы, но так не работает. С [1] находятся все такие классы. С любым другим индексом не
//        находится ничего, хотя в папке на момент проверки их было 19 штук..
//        WebElement savedEmail = driver.findElement(By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject'][1]"));
//        System.out.println(savedEmail.getText());

//        Verify контент, адресата и тему письма (должно совпадать с пунктом 3)
        wait.until(elementToBeClickable(By.xpath("//span[@title='Проверить, есть ли новые письма (F9)']"))).click();

      //  WebElement itemBody = wait.until(elementToBeClickable(By.className("js-message-snippet-sender")));
        WebElement itemBody = wait.until(elementToBeClickable(By.xpath("//span[contains(@class,'js-message-snippet-sender')]")));
        catchStaleExceptions(itemBody, ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(@class,'js-message-snippet-sender')]")));

        WebElement recipient = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ComposeYabble-Text")));
        Assert.assertEquals(recipient.getText(), "feelena2020@yandex.ru");

        WebElement themeEmail = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("mail-MessageSnippet-Item_subject")));
        Assert.assertEquals(themeEmail.getText(), "Homework_5");

        WebElement emailBody = driver.findElement(By.className("cke_contents_ltr"));
        Assert.assertEquals(emailBody.getText(), "Trying hard..");

//        Отправить письмо
        wait.until(elementToBeClickable(By.className("ComposeControlPanelButton-Button_action"))).click();

//        Verify, что письмо исчезло из черновиков
        wait.until(elementToBeClickable(By.linkText("Черновики"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("b-messages__placeholder-item")));
        WebElement emptyDraft = driver.findElement(By.className("b-messages__placeholder-item"));
        System.out.println(emptyDraft.getText());
        assertTrue(emptyDraft.getText().equals("В папке «Черновики» нет писем."));


//        Verify, что письмо появилось в папке отправленные
        WebElement buttonSent = driver.findElement(By.linkText("Отправленные"));
        catchStaleExceptions(buttonSent, ExpectedConditions.elementToBeClickable(By.linkText("Отправленные")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")));
        List<WebElement> listOfEmailsByThemeSent = driver.findElements(By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']"));
        System.out.println(listOfEmailsByThemeSent.get(0).getText());
        assertTrue(listOfEmailsByThemeSent.get(0).getText().equals("Homework_5"));

//        Удалить письмо
        WebElement selectAll = driver.findElement(By.className("checkbox_view"));
        selectAll.click();

        WebElement delete = driver.findElement(By.className("js-toolbar-item-title-delete"));
        delete.click();
    }
}
