package homework_8;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeMethod;
import static io.restassured.RestAssured.oauth2;

public abstract class BaseClass {

    RequestSpecification rqSpec;
    ResponseSpecification rsSpec;
    String token = "n5cKd7OUnMfb502dTXMQVOhvI6Sg_voQvu15";

    @BeforeMethod
    public void setUp() {
        rqSpec = new RequestSpecBuilder()
                .setBaseUri(Endpoints.BASE_URL)
                .setAuth(oauth2(token))
                .log(LogDetail.ALL)
                .build();
    }
}

