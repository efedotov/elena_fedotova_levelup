package homework_8;

import homework_8_rest_assured.*;
import io.qameta.allure.Description;
import io.restassured.http.ContentType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class Posts extends BaseClass {

    @Description("GET /public-api/posts: list all posts")
    @Test(dataProviderClass = DataProvider.class, dataProvider = "metaInfoData")
    public void getListAllPosts(Meta meta) {
        ListPostResponse getAllPostsResponse =
                given()
                        .spec(rqSpec)
                        .queryParam("page", 101)
                        .when()
                        .get(Endpoints.POSTS)
                        .as(ListPostResponse.class);
        System.out.println(getAllPostsResponse);
        assertThat(getAllPostsResponse.getMeta(), samePropertyValuesAs(meta));
    }

    @Description("GET /public-api/posts?title=Quasi nobis eos est autem")
    @Test  (dataProviderClass = DataProvider.class, dataProvider = "getMetaTitle")
    public void getPostTitle(Meta meta, String title) {
        ListPostResponse getPostsResponse =
                given()
                        .spec(rqSpec)
                        .queryParam("title", "Quasi nobis eos est autem")
                        .when()
                        .get(Endpoints.POSTS)
                        .as(ListPostResponse.class);

        System.out.println(getPostsResponse);
        assertThat(getPostsResponse.getMeta(), samePropertyValuesAs(meta));
        getPostsResponse.getResult().forEach(element -> assertThat(element.getTitle(),containsString(title)));
    }

    @Description("POST, GET, PUT, DELETE new post")
    @Test  (dataProviderClass = DataProvider.class, dataProvider = "newPost")
    public void postGePutDeleteNewPost (Meta metaPost, Post myPost, Meta metaGetPatch, Post myNewPost, Meta metaDelete) {
        PostResponse postBody = given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(myPost)
                .when()
                .post(Endpoints.POSTS)
                .as(PostResponse.class);
        System.out.println(postBody);
        assertThat(postBody.getMeta(), samePropertyValuesAs(metaPost));
        assertThat(postBody.getResult().getId(), notNullValue());
        assertThat(postBody.getResult(), samePropertyValuesAs(myPost, "id"));

        PostResponse getBody = given()
                .spec(rqSpec)
                .pathParam("id", postBody.getResult().getId())
                .contentType(ContentType.JSON)
                .when()
                .get(Endpoints.POSTS + "/{id}")
                .as(PostResponse.class);
        System.out.println(getBody);
        assertThat(getBody.getMeta(), samePropertyValuesAs(metaGetPatch));
        assertThat(getBody.getResult().getId(), equalTo(postBody.getResult().getId()));
        assertThat(getBody.getResult(), samePropertyValuesAs(myPost, "id"));

        PostResponse putBody = given()
                .spec(rqSpec)
                .pathParam("id", postBody.getResult().getId())
                .contentType(ContentType.JSON)
                .body(myNewPost)
                .when()
                .put(Endpoints.POSTS + "/{id}")
                .as(PostResponse.class);
        System.out.println(putBody);
        assertThat(putBody.getMeta(), samePropertyValuesAs(metaGetPatch));
        assertThat(putBody.getResult().getId(), equalTo(postBody.getResult().getId()));
        assertThat(putBody.getResult(), samePropertyValuesAs(myNewPost, "id"));


        PostResponse deleteBody = given()
                .spec(rqSpec)
                .pathParam("id", postBody.getResult().getId())
                .contentType(ContentType.JSON)
                .body("")
                .when()
                .delete(Endpoints.POSTS + "/{id}")
                .as(PostResponse.class);
        System.out.println(deleteBody);
        assertThat(deleteBody.getMeta(), samePropertyValuesAs(metaDelete));
        assertThat(deleteBody.getResult(), nullValue());
    }
}

