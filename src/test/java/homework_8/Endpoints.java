package homework_8;

public final class Endpoints {
    public static final String BASE_URL = "https://gorest.co.in/";

    public static final String USERS = "/public-api/users";

    public static final String POSTS = "/public-api/posts";

    public static final String COMMENTS = "/public-api/comments";

    private Endpoints() {
    }
}
