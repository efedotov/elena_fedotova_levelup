package homework_8;


import homework_8_rest_assured.*;
import io.qameta.allure.Description;
import io.restassured.http.ContentType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class Users extends BaseClass {

    @Description("GET /public-api/users: list all users")
    @Test(dataProviderClass = DataProvider.class, dataProvider = "metaInfoData")
    public void getListAllUsers(Meta meta) {
        ListUsersResponse usersResponse =
                given()
                        .spec(rqSpec)
                        .queryParam("page", 99)
                        .when()
                        .get(Endpoints.USERS)
                        .as(ListUsersResponse.class);
        System.out.println(usersResponse);
        assertThat(usersResponse.getMeta().isSuccess(), equalTo(meta.isSuccess()));
        assertThat(usersResponse.getMeta().getCode(), equalTo(meta.getCode()));
        assertThat(usersResponse.getMeta().getMessage(), equalTo(meta.getMessage()));
    }

    @Description("GET /public-api/users?first_name=Noe: list all users with first_name contains Noe")
    @Test  (dataProviderClass = DataProvider.class, dataProvider = "metaInfoData")
    public void getUserNoe(Meta meta) {
        ListUsersResponse usersResponse =
                given()
                        .spec(rqSpec)
                        .when()
                        .get("/public-api/users?first_name=Noe")
                        .as(ListUsersResponse.class);
        System.out.println(usersResponse);
        assertThat(usersResponse.getMeta().isSuccess(), equalTo(meta.isSuccess()));
        assertThat(usersResponse.getMeta().getCode(), equalTo(meta.getCode()));
        assertThat(usersResponse.getMeta().getMessage(), equalTo(meta.getMessage()));

        usersResponse.getResult().forEach(element -> assertThat(element.getFirstName(),containsString("Noe")));
        usersResponse.getResult().forEach(element -> System.out.println(element.getFirstName()));
    }


    @Description("POST, GET, PATCH, DELETE new user")
    @Test  (dataProviderClass = DataProvider.class, dataProvider = "newUser")
    public void postGetPatchDeleteNewUser (Meta metaPost, User nita, Meta metaGetPatch, User newNita, Meta metaDelete) {
        UserResponse postBody = given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(nita)
                .when()
                .post(Endpoints.USERS)
                .as(UserResponse.class);
        System.out.println(postBody);
        assertThat(postBody.getMeta(), samePropertyValuesAs(metaPost));
        assertThat(postBody.getResult().getId(), notNullValue());
        assertThat(postBody.getResult().getFirstName(),equalTo(nita.getFirstName()));
        assertThat(postBody.getResult().getLastName(),equalTo(nita.getLastName()));

        UserResponse getBody = given()
                .spec(rqSpec)
                .pathParam("id", postBody.getResult().getId())
                .contentType(ContentType.JSON)
                .when()
                .get(Endpoints.USERS + "/{id}")
                .as(UserResponse.class);
        System.out.println(getBody);
        assertThat(getBody.getMeta(), samePropertyValuesAs(metaGetPatch));
        assertThat(getBody.getResult().getId(), equalTo(postBody.getResult().getId()));
        assertThat(getBody.getResult(), samePropertyValuesAs(nita, "id"));

        UserResponse patchBody = given()
                .spec(rqSpec)
                .pathParam("id", postBody.getResult().getId())
                .contentType(ContentType.JSON)
                .body(newNita)
                .when()
                .patch(Endpoints.USERS + "/{id}")
                .as(UserResponse.class);
        System.out.println(patchBody);
        assertThat(patchBody.getMeta(), samePropertyValuesAs(metaGetPatch));
        assertThat(patchBody.getResult().getId(), equalTo(postBody.getResult().getId()));
        assertThat(patchBody.getResult(), samePropertyValuesAs(newNita, "id"));

        UserResponse deleteBody = given()
                .spec(rqSpec)
                .pathParam("id", postBody.getResult().getId())
                .contentType(ContentType.JSON)
                .body("")
                .when()
                .delete(Endpoints.USERS + "/{id}")
                .as(UserResponse.class);
        System.out.println(deleteBody);
        assertThat(deleteBody.getMeta(), samePropertyValuesAs(metaDelete));
        assertThat(deleteBody.getResult(), nullValue());
    }
}
