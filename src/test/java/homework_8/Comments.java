package homework_8;


import homework_8_rest_assured.*;
import io.qameta.allure.Description;
import io.restassured.http.ContentType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class Comments extends BaseClass {

    @Description("GET /public-api/comments: list all comments.")
    @Test(dataProviderClass = DataProvider.class, dataProvider = "metaInfoData")
    public void getListAllComments(Meta meta) {
        ListCommentResponse getAllCommentsResponse =
                given()
                        .spec(rqSpec)
                        .queryParam("page", 101)
                        .when()
                        .get(Endpoints.COMMENTS)
                        .as(ListCommentResponse.class);
        System.out.println(getAllCommentsResponse);
        assertThat(getAllCommentsResponse.getMeta(), samePropertyValuesAs(meta));
    }


    @Description("GET /public-api/comments?Name=Montana: list all comments with title contains Hassie.")
    @Test  (dataProviderClass = DataProvider.class, dataProvider = "getMetaName")
    public void getCommentName(Meta meta, String name) {
        ListCommentResponse getCommentsResponse =
                given()
                        .spec(rqSpec)
                        .queryParam("name", "Hassie")
                        .when()
                        .get(Endpoints.COMMENTS)
                        .as(ListCommentResponse.class);

        System.out.println(getCommentsResponse);
        assertThat(getCommentsResponse.getMeta(), samePropertyValuesAs(meta));
        getCommentsResponse.getResult().forEach(element -> assertThat(element.getName(),containsString(name)));
    }

    @Description("POST, GET, PATCH, DELETE new comment")
    @Test  (dataProviderClass = DataProvider.class, dataProvider = "newComment")
    public void postGetPatchDeleteNewComment (Meta metaPost, Comment myComment, Meta metaGetPatch, Comment myNewComment, Meta metaDelete) {
        CommentResponse postBody = given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(myComment)
                .when()
                .post(Endpoints.COMMENTS)
                .as(CommentResponse.class);
        System.out.println(postBody);
        assertThat(postBody.getMeta(), samePropertyValuesAs(metaPost));
        assertThat(postBody.getResult().getId(), notNullValue());
        assertThat(postBody.getResult(), samePropertyValuesAs(myComment, "id"));

        CommentResponse getBody = given()
                .spec(rqSpec)
                .pathParam("id", postBody.getResult().getId())
                .contentType(ContentType.JSON)
                .when()
                .get(Endpoints.COMMENTS + "/{id}")
                .as(CommentResponse.class);
        System.out.println(getBody);
        assertThat(getBody.getMeta(), samePropertyValuesAs(metaGetPatch));
        assertThat(getBody.getResult().getId(), equalTo(postBody.getResult().getId()));
        assertThat(getBody.getResult(), samePropertyValuesAs(myComment, "id"));

        CommentResponse patchBody = given()
                .spec(rqSpec)
                .pathParam("id", postBody.getResult().getId())
                .contentType(ContentType.JSON)
                .body(myNewComment)
                .when()
                .put(Endpoints.COMMENTS + "/{id}")
                .as(CommentResponse.class);
        System.out.println(patchBody);
        assertThat(patchBody.getMeta(), samePropertyValuesAs(metaGetPatch));
        assertThat(patchBody.getResult().getId(), equalTo(postBody.getResult().getId()));
        assertThat(patchBody.getResult(), samePropertyValuesAs(myNewComment, "id"));

        CommentResponse deleteBody = given()
                .spec(rqSpec)
                .pathParam("id", postBody.getResult().getId())
                .contentType(ContentType.JSON)
                .body("")
                .when()
                .delete(Endpoints.COMMENTS + "/{id}")
                .as(CommentResponse.class);
        System.out.println(deleteBody);
        assertThat(deleteBody.getMeta(), samePropertyValuesAs(metaDelete));
        assertThat(deleteBody.getResult(), nullValue());
    }
}

