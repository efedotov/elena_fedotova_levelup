package homework_8;

import homework_8_rest_assured.Comment;
import homework_8_rest_assured.Meta;
import homework_8_rest_assured.Post;
import homework_8_rest_assured.User;
import org.testng.annotations.DataProvider;

public class DataProviders {
    Meta metaGetPatch = new Meta (true, 200, "OK. Everything worked as expected.");
    Meta metaPost = new Meta (true, 201, "A resource was successfully created in response to a POST request." +
            " The Location header contains the URL pointing to the newly created resource.");
    Meta metaDelete = new Meta (true, 204, "The request was handled successfully and the response contains no body content.");
    User nita = new User ("Nita", "Ebbers", "female", "nita@gmail.ua", "1980-11-15", "868-588-8888");
    User newNita = new User ("Nita", "Lamberg", "female", "nitaLamberg@gmail.ua",
            "1980-11-15", "868-588-8888");
    Post myPost = new Post (29859, "something", "blueberry pie");
    Post myNewPost = new Post (29859, "anything else", "strawberry pie");
    Comment myComment = new Comment(6990, "Dr. Murphy", "ed.murphy@mail.ru", "testBody");
    Comment myNewComment = new Comment(6990, "Dr. Shaun", "ed.shaun@mail.ru", "NewTestBody");


    @DataProvider
    public Object[][] metaInfoData () {
        return new Object [][] {
                {metaGetPatch}
        };
    }

    @DataProvider
    public Object[][] newUser() {
        return new Object[][] {
                {metaPost, nita, metaGetPatch, newNita, metaDelete}
        };
    }

    @DataProvider
    public Object[][] getMetaTitle () {
        return new Object [][] {
                {metaGetPatch, "Quasi nobis eos est autem"}
        };
    }

    @DataProvider
    public Object[][] newPost () {
        return new Object [][] {
                {metaPost, myPost, metaGetPatch, myNewPost, metaDelete}
        };
    }

    @DataProvider
    public Object[][] getMetaName () {
        return new Object [][] {
                {metaGetPatch, "Hassie"}
        };
    }

    @DataProvider
    public Object[][] newComment () {
        return new Object [][] {
                {metaPost, myComment, metaGetPatch, myNewComment, metaDelete}
        };
    }
}


