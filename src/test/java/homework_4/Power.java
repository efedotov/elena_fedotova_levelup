package homework_4;

import homework_4.dataProviders.RestDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Power extends GeneralCalculatorTest {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest Power");
    }


    @Test(dataProviderClass = RestDataProvider.class, dataProvider = "powDataProvider")
    public void power (double a, double b, double expected) {
        double result = calculator.pow(a,b);
        Assert.assertEquals(result, expected, 0.00001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest Power");
    }

}
