package homework_4;

import homework_4.dataProviders.RestDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SquareRoot extends GeneralCalculatorTest {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest SquareRoot");
    }


    @Test(dataProviderClass = RestDataProvider.class, dataProvider = "sqrtDataProvider")
    public void squareRootTest (double a, double expected) {
        double result = calculator.sqrt(a);
        Assert.assertEquals(result, expected, 0.00001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest SquareRoot");
    }
}
