package homework_4;

import homework_4.dataProviders.RestDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Positive extends GeneralCalculatorTest{

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest isPositive");
    }


    @Test(dataProviderClass = RestDataProvider.class, dataProvider = "isPositiveDataProvider")
    public void isPositive (long a, boolean expected) {
        boolean result = calculator.isPositive(a);
        Assert.assertEquals(result, expected);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest isPositive");
    }
}
