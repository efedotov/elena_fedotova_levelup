package homework_4;

import homework_4.dataProviders.TrigonometryDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Cotangent extends GeneralCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest Cotangent");
    }


    @Test(dataProviderClass = TrigonometryDataProvider.class, dataProvider = "сtgDataProvider")
    public void cotangentTest(double a, double expected) {
        double result = calculator.ctg(a);
        Assert.assertEquals(result, expected, 0.00001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest Cotangent");
    }

}
