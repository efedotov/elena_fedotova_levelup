package homework_4;

import homework_4.dataProviders.CalculationDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Subtraction extends GeneralCalculatorTest {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest Subtraction");
    }

    @Test (dataProviderClass = CalculationDataProvider.class, dataProvider = "subtractionDataProviderDouble")
    public void subtractDoubleTest(double a, double b, double expected) {
        double result = calculator.sub(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @Test (dataProviderClass = CalculationDataProvider.class, dataProvider = "subtractionDataProviderLong")
    public void subtractLongTest(long a, long b, long expected) {
        long result = (long) calculator.sub(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest Subtraction");
    }
}
