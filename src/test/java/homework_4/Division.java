package homework_4;

import homework_4.dataProviders.CalculationDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Division extends GeneralCalculatorTest {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest Division");
    }


    @Test(dataProviderClass = CalculationDataProvider.class, dataProvider = "divisionDataProviderDouble")
    public void divideDoubleTest(double a, double b, double expected) {
        double result = calculator.div(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @Test (dataProviderClass = CalculationDataProvider.class, dataProvider = "divisionDataProviderLong")
    public void divideLongTest(long a, long b, long expected) {
        long result = (long) calculator.div(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest Division");
    }
}
