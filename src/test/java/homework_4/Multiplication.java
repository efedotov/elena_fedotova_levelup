package homework_4;

import homework_4.dataProviders.CalculationDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Multiplication extends GeneralCalculatorTest {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest Multiplication");
    }


    @Test(dataProviderClass = CalculationDataProvider.class, dataProvider = "multiplicationDataProviderDouble")
    public void multiplyDoubleTest(double a, double b, double expected) {
        double result = calculator.mult(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @Test (dataProviderClass = CalculationDataProvider.class, dataProvider = "multiplicationDataProviderLong")
    public void multiplyLongTest(long a, long b, long expected) {
        long result = (long) calculator.mult(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest Multiplication");
    }
}
