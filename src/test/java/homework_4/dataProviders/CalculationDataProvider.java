package homework_4.dataProviders;

import org.testng.annotations.DataProvider;

public class CalculationDataProvider {


    private Object IllegalArgumentException;

    @DataProvider
    public Object[][] additionDataProviderDouble () {
        return new Object [][] {
                {12.0, 24.0, 36.0},
                {0.0, 55.64, 55.64},
                {11869.0, 11.65, 11880.65}
        };
    }

    @DataProvider
    public Object[][] additionDataProviderLong () {
         return new Object [][] {
                {2147483649L, 3147483888L, 5294967537L},
                 {0L, 2147483649L, 2147483649L}
             };
    }


    @DataProvider
    public Object[][] subtractionDataProviderDouble () {
        return new Object [][] {
                {108.12, 8.03, 100.09},
                {506, 500, 6}
        };
    }

    @DataProvider
    public Object[][] subtractionDataProviderLong () {
        return new Object [][] {
                {7341474836491L, 871474838889L, 6469999997602L},
                {4555147483649L, 1714748388L, 4553432735261L},
               };
    }

    @DataProvider
    public Object[][] multiplicationDataProviderDouble () {
        return new Object [][] {
                {16.9, 25.08, 423.852},
                {0.0, 1500.0, 0.0}
        };
    }

    @DataProvider
    public Object[][] multiplicationDataProviderLong () {
        return new Object [][] {
                {7341474836490L, 8L, 58731798691920L}
                };
    }

    @DataProvider
    public Object[][] divisionDataProviderDouble () {
        return new Object [][] {
                {49.0, 7.0, 7.0},
                {50.5, 10.0, 5.05},
                {150.0, 0.0, Double.POSITIVE_INFINITY}
             };
    }

    @DataProvider
    public Object[][] divisionDataProviderLong () {
        return new Object [][] {
                {7341474836490L, 5L, 1468294967298L},
                {734147483676490L, 0L, IllegalArgumentException}
        };
    }
}
