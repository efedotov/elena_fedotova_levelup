package homework_4.dataProviders;

import org.testng.annotations.DataProvider;

public class TrigonometryDataProvider {

    @DataProvider
    public Object[][] sinDataProvider() {
        return new Object[][]{
                {1.5708, 1.0}, // 90 gr
                {0.523599, 0.5} // 30 gr
        };
    }

    @DataProvider
    public Object[][] cosDataProvider() {
        return new Object[][]{
                {1.5708, 0.0}, // 90 gr
                {1.0472, 0.5} // 60 gr
        };
    }

    @DataProvider
    public Object[][] tgDataProvider() {
        return new Object[][]{
                {0.785398, 1.0}, // 45 gr
                {3.14159, 0.0}, // 180 gr
                {1.0472, 1.73205080}, // 60 gr
                {0.523599, 0.5773502} // 30 gr
        };
    }

    @DataProvider
    public Object[][] сtgDataProvider() {
        return new Object[][]{
                {0.785398, 1.0}, // 45 gr
                {1.0472, 0.57735}  // 60 gr
        };
    }
}
