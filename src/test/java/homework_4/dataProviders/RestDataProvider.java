package homework_4.dataProviders;

import org.testng.annotations.DataProvider;

public class RestDataProvider {

    @DataProvider
    public Object[][] powDataProvider() {
        return new Object[][]{
                {7.0, 7.0, 823543.0},
                {8.5, 3.0, 614.125}
        };
    }

    @DataProvider
    public Object[][] sqrtDataProvider() {
        return new Object[][]{
                {125.0, 11.180339},
                {225.0, 15.0}
        };
    }

    @DataProvider
    public Object[][] isPositiveDataProvider() {
        return new Object[][]{
                {0, false},
                {-5, false},
                {87, true}
        };
    }

    @DataProvider
    public Object[][] isNegativeDataProvider() {
        return new Object[][]{
                {0, false},
                {-5, true},
                {87, false}
        };
    }

}
