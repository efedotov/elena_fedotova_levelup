package homework_4;

import homework_4.dataProviders.TrigonometryDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Sinus extends GeneralCalculatorTest {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest Sinus");
    }


    @Test(dataProviderClass = TrigonometryDataProvider.class, dataProvider = "sinDataProvider")
    public void sinusTest(double a, double expected) {
        double result = calculator.sin(a);
        Assert.assertEquals(result, expected, 0.000001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest Sinus");
    }

}