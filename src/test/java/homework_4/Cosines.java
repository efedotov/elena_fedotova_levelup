package homework_4;

import homework_4.dataProviders.TrigonometryDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Cosines extends GeneralCalculatorTest {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest Cosines");
    }


    @Test(dataProviderClass = TrigonometryDataProvider.class, dataProvider = "cosDataProvider")
    public void cosinesTest(double a, double expected) {
        double result = calculator.cos(a);
        Assert.assertEquals(result, expected, 0.000001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest Cosines");
    }
}
