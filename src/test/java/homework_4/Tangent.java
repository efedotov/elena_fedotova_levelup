package homework_4;

import homework_4.dataProviders.TrigonometryDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Tangent extends GeneralCalculatorTest {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest Tangent");
    }


    @Test(dataProviderClass = TrigonometryDataProvider.class, dataProvider = "tgDataProvider")
    public void tangentTest(double a, double expected) {
        double result = calculator.tg(a);
        Assert.assertEquals(result, expected, 0.000001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest Tangent");
    }
}
