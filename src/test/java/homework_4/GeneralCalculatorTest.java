package homework_4;

import org.testng.annotations.*;
import com.epam.tat.module4.Calculator;


public abstract class GeneralCalculatorTest {
    protected Calculator calculator;

    @BeforeSuite
    public void setUpBeforeSuite() {
        System.out.println("setUpBeforeSuite");
    }


    @BeforeClass
    public void setUpBeforeClass() {
        System.out.println("setUpBeforeClass");
     }

    @BeforeMethod
    public void setUpBeforeMethod () {
        System.out.println("setUpBeforeMethod");
        calculator = new Calculator();
    }

    @AfterMethod
    public void tearDownAfterMethod() {
        System.out.println("tearDownAfterMethod");
        calculator = null;
    }

    @AfterClass
    public void tearDownAfterClass() {
        System.out.println("tearDownAfterClass");
    }

    @AfterSuite
    public void tearDownAfterSuite() {
        System.out.println("tearDownAfterSuite");
    }

}
