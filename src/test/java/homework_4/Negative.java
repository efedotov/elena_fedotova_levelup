package homework_4;

import homework_4.dataProviders.RestDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Negative extends  GeneralCalculatorTest {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest isNegative");
    }


    @Test(dataProviderClass = RestDataProvider.class, dataProvider = "isNegativeDataProvider")
    public void isNegative (long a, boolean expected) {
        boolean result = calculator.isNegative(a);
        Assert.assertEquals(result, expected);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest isNegative");
    }
}
