package selenium.scripts;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

import static java.lang.Thread.sleep;
import static org.testng.Assert.assertEquals;

public class LocatorTest {
    private WebDriver driver;

    @BeforeMethod
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://gmail.com");

    }

        @Test
    public void seleniumDomLocatorTextTest() throws InterruptedException {
            WebElement userNameTextFieldById = driver.findElement(By.id("identifierId"));
            userNameTextFieldById.sendKeys("test email");
            sleep(3000);

            WebElement userNameTextFieldByName =   driver.findElement(By.name("identifier"));
            userNameTextFieldByName.clear();
            userNameTextFieldByName.sendKeys("my new email");
            sleep(3000);

           List<WebElement> listByClassName = driver.findElements(By.className("PrDSKc"));
           for (WebElement webElement : listByClassName) {
               System.out.println(webElement.getText());
           }
            System.out.println();
            sleep(3000);

            WebElement linkByLinkText = driver.findElement(By.linkText("Подробнее…"));
            System.out.println(linkByLinkText.getText());
            System.out.println();

            WebElement linkByPartialLinkText = driver.findElement(By.partialLinkText("одроб"));
            System.out.println(linkByPartialLinkText.getText());
            System.out.println();
        }

//    @Test
//    public void seleniumXPathLocatorTextTest() throws InterruptedException {
//        WebElement userNameTextFieldById = driver.findElement(By.xpath("//*[@id='identifierId']"));
//        userNameTextFieldById.sendKeys("test email");
//        sleep(3000);
//
//        WebElement userNameTextFieldByName =   driver.findElement(By.xpath("//input[@name='identifier']"));
//        userNameTextFieldByName.clear();
//        userNameTextFieldByName.sendKeys("my new email");
//        sleep(3000);
//
//        List<WebElement> listByClassName = driver.findElements(By.cssSelector(".PrDSKc"));
//        for (WebElement webElement : listByClassName) {
//            System.out.println(webElement.getText());
//        }
//        System.out.println();
//        sleep(3000);
//
//        WebElement linkByLinkText = driver.findElement(By.xpath("//a[text()='Learn more']"));
//        System.out.println(linkByLinkText.getText());
//        System.out.println();
//
//        WebElement linkByPartialLinkText = driver.findElement(By.xpath("//a[.='Learn more']")); //тоже самое, что //a[text()='Learn more']
//        System.out.println(linkByPartialLinkText.getText());
//        System.out.println();
//    }

    @Test
    public void seleniumCSSLocatorTextTest() throws InterruptedException {
        WebElement userNameTextFieldById = driver.findElement(By.cssSelector("#identifierId"));
        userNameTextFieldById.sendKeys("test email");
        sleep(3000);

        WebElement userNameTextFieldByName =   driver.findElement(By.cssSelector("[name='identifier']"));
        userNameTextFieldByName.clear();
        userNameTextFieldByName.sendKeys("my new email");
        sleep(3000);

        List<WebElement> listByClassName = driver.findElements(By.xpath("//*[@class='PrDSKc']")); // //div[contains(@class, 'rFrNMe')]
        for (WebElement webElement : listByClassName) {
            System.out.println(webElement.getText());
        }
        System.out.println();
        sleep(3000);

        WebElement linkByLinkText = driver.findElement(By.cssSelector("div a[jsname='JFyozc']"));
        System.out.println(linkByLinkText.getText());
        System.out.println();

        WebElement linkByPartialLinkText = driver.findElement(By.cssSelector("div [jsname='FIbd0b'] span span.snByac"));
        System.out.println(linkByPartialLinkText.getText());
        System.out.println();
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }


}
