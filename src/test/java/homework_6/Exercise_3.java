package homework_6;

import org.testng.Assert;
import org.testng.annotations.Test;
import po_homework_6.Bin;
import po_homework_6.Drafts;
import po_homework_6.Incoming;
import po_homework_6.YandexMailInbox;
import static org.testng.Assert.assertEquals;

public class Exercise_3 extends BaseTest {

    @Test
    public void exercise3() {
        YandexMailInbox yandexMailInbox = new YandexMailInbox(driver);
        Drafts draft = new Drafts(driver);
        Incoming incoming = new Incoming(driver);
        Bin bin = new Bin(driver);
        String userMail = property.getProperty("user.email");
        String emailTheme = property.getProperty("theme3");
        String emailBody = property.getProperty("content3");

//           Создать новое письмо (заполнить адресата (самого себя), тему письма (должно содержать слово Тест) и тело)
        yandexMailInbox.writeEmail(userMail, emailTheme, emailBody);

//                  Отправить письмо
        draft.sendEmail();

//                  Verify, что письмо появилось в папке Входящие
        incoming.goToIncoming();
       assertEquals(incoming.readIncoming(), (emailTheme));

//           Verify контент, адресата и тему письма (должно совпадать с пунктом 3), зайти в письмо
        incoming.openEmail();
        Assert.assertEquals(incoming.readRecipient(), userMail);
        Assert.assertEquals(incoming.readThemeEmail(), emailTheme);
        Assert.assertEquals(incoming.readEmailBody(), emailBody);

//        Удалить письмо
        incoming.cleanUpIncomingBox();

//                Verify что письмо появилось в папке Корзина
        bin.goToBin();
        Assert.assertEquals(bin.readBin(), emailTheme);
    }
}
