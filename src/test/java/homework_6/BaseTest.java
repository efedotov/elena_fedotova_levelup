package homework_6;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import po_homework_6.YandexMailInbox;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import static org.testng.Assert.assertEquals;

public abstract class BaseTest {
    protected WebDriver driver;
    protected WebDriverWait wait;
    Properties property = new Properties();

    @BeforeMethod
     public void openMailbox () throws IOException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 60);
        driver.manage().window().maximize();
        YandexMailInbox yandexMailInbox = new YandexMailInbox(driver);

        FileInputStream fis = new FileInputStream("src/test/resources/config.properties");
        property.load(fis);
        String login = property.getProperty("email.login");
        String password = property.getProperty("email.pass");

//        проверили, что зашли на нужный сайт
        yandexMailInbox.open();
        String title = driver.getTitle();
        assertEquals(title, "Яндекс.Почта — бесплатная и надежная электронная почта");
        //      или
//      String a = "Яндекс";
//      Assert.assertTrue(title.contains(a));

        yandexMailInbox.login(login, password);

//        Assert, что вход выполнен успешно
        WebElement userIcon = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("user-account__name")));
        AssertJUnit.assertEquals(userIcon.getText(), login);
    }

    @AfterMethod
    public void quitMailbox() {
        driver.quit();
    }

}
