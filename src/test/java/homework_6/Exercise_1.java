package homework_6;

import org.testng.Assert;
import org.testng.annotations.Test;
import po_homework_6.Drafts;
import po_homework_6.Sent;
import po_homework_6.YandexMailInbox;
import static org.testng.Assert.assertTrue;

public class Exercise_1 extends BaseTest {

    @Test
    public void exercise1() {
        YandexMailInbox yandexMailInbox = new YandexMailInbox(driver);
        Drafts draft = new Drafts(driver);
        Sent sent = new Sent(driver);
        String userMail = property.getProperty("user.email");
        String emailTheme = property.getProperty("theme1");
        String emailBody = property.getProperty("content1");

//        Создать новое письмо (заполнить адресата, тему письма и тело)
        yandexMailInbox.writeEmail(userMail, emailTheme, emailBody);

//        Сохранить его как черновик
        yandexMailInbox.saveAsDraft();

//        Verify, что письмо сохранено в черновиках
        draft.goToDraft();
        assertTrue(draft.readDraft().equals(emailTheme));


//        Verify контент, адресата и тему письма (должно совпадать с пунктом 3)
        draft.openDraft();
        assertTrue(draft.readRecipient().equals(userMail));
        assertTrue(draft.readThemeEmail().equals(emailTheme));
        assertTrue(draft.readEmailBody().equals(emailBody));

//        Отправить письмо
        draft.sendEmail();

//        Verify, что письмо исчезло из черновиков
        Assert.assertEquals(draft.emptiedDraft(), "В папке «Черновики» нет писем.");

//        Verify, что письмо появилось в папке отправленные
        sent.goToSentBox();
        assertTrue(sent.refreshSendBox().equals(emailTheme));

//        Удалить письмо
        sent.deleteEmail();
    }
}
