package homework_7;

import com.codeborne.selenide.Configuration;
import homework_7_selenide.Drafts;
import homework_7_selenide.YandexMailInbox;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.assertEquals;

public abstract class BaseTest {
    Properties property = new Properties();
    YandexMailInbox yandexMailInbox;
    Drafts draft;

    @BeforeMethod
    public void openMailbox() throws IOException {
        Configuration.browser = "CHROME";
        Configuration.startMaximized = true;
        Configuration.timeout = 17000;

        yandexMailInbox= new YandexMailInbox().open();

        FileInputStream fis = new FileInputStream("src/test/resources/config.properties");
        property.load(fis);
        String login = property.getProperty("email.login");
        String password = property.getProperty("email.pass");

//        проверили, что зашли на нужный сайт
//                yandexMailInbox.open();
        Assert.assertEquals(title(), "Яндекс.Почта — бесплатная и надежная электронная почта");
        yandexMailInbox.login(login, password);

//        Assert, что вход выполнен успешно
        $(".user-account__name").shouldHave(text(login));
    }

    @AfterMethod
    public void closeMailBox() {
        closeWebDriver();
    }

}

