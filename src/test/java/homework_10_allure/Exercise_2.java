package homework_10_allure;

import homework_10.Drafts;
import homework_10.Sent;
import homework_10.TestBox;
import homework_10.YandexMailInbox;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class Exercise_2 extends BaseTest {
    @Test
    public void exercise2()  {
        YandexMailInbox yandexMailInbox = new YandexMailInbox().open();
        Drafts draft = new Drafts().open();
        Sent sent = new Sent().open();
        TestBox testBox = new TestBox().open();
        String userMail = property.getProperty("user.email");
        String emailTheme = property.getProperty("theme2");
        // String emailTheme = new String (property.getProperty("theme2").getBytes("ISO8859-1"));
        String emailBody = property.getProperty("content2");

//        Создать новое письмо (заполнить адресата (самого себя), тему письма (должно содержать слово Тест) и тело)
        yandexMailInbox.writeEmail(userMail, emailTheme, emailBody);

//          Отправить письмо
        draft.sendEmail();

//        Verify, что письмо появилось в папке отправленные
        sent.goToSentBox();
        assertTrue(sent.refreshSendBox().equals(emailTheme));

//        Verify, что письмо появилось в папке «Тест»
        testBox.goToTestBox();

//        Verify контент, адресата и тему письма (должно совпадать с пунктом 3)
        testBox.openEmailInTestBox();
        Assert.assertEquals(testBox.readRecipient(), userMail);

        Assert.assertEquals(testBox.readThemeEmail(), emailTheme);
        Assert.assertEquals(testBox.readEmailBody(), emailBody);

//               Удалить письмо из папки "Тест"
        testBox.cleanUpTestBox();
    }
}
