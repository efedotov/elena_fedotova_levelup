package homework_8_rest_assured;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class CommentResponse extends BaseClass {
    @SerializedName("result")
    private Comment result;

    public CommentResponse(Meta meta, Comment result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Comment getResult() {
        return result;
    }

    public void setResult(Comment result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommentResponse)) return false;
        CommentResponse that = (CommentResponse) o;
        return Objects.equals(getMeta(), that.getMeta()) &&
                Objects.equals(getResult(), that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeta(), getResult());
    }

    @Override
    public String toString() {
        return "CommentResponse{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}

