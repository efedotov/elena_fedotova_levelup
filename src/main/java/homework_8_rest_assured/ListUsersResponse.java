package homework_8_rest_assured;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class ListUsersResponse extends BaseClass {
    @SerializedName("result")
    private List<User> result;

    public ListUsersResponse(Meta meta, List<User> result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public List<User> getResult() {
        return result;
    }

    public void setResult(List<User> result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListUsersResponse)) return false;
        ListUsersResponse that = (ListUsersResponse) o;
        return Objects.equals(getMeta(), that.getMeta()) &&
                Objects.equals(getResult(), that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeta(), getResult());
    }

    @Override
    public String toString() {
        return "ListUsersResponse{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}

