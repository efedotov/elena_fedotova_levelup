package homework_8_rest_assured;


import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class ListPostResponse extends BaseClass {
    @SerializedName("result")
    private List<Post> result;

    public ListPostResponse(Meta meta, List<Post> result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public List<Post> getResult() {
        return result;
    }

    public void setResult(List<Post> result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListPostResponse)) return false;
        ListPostResponse that = (ListPostResponse) o;
        return Objects.equals(getMeta(), that.getMeta()) &&
                Objects.equals(getResult(), that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeta(), getResult());
    }

    @Override
    public String toString() {
        return "ListPostResponse{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}

