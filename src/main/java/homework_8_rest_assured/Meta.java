package homework_8_rest_assured;

import java.util.Objects;

public class Meta {
    private boolean success;
    private int code;
    private String message;

    public Meta(boolean success, int code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Meta)) return false;
        Meta meta = (Meta) o;
        return isSuccess() == meta.isSuccess() &&
                getCode() == meta.getCode() &&
                Objects.equals(getMessage(), meta.getMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isSuccess(), getCode(), getMessage());
    }

    @Override
    public String toString() {
        return "Meta{" +
                "success=" + success +
                ", code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}

