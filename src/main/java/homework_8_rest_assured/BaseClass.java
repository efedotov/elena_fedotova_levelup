package homework_8_rest_assured;

import com.google.gson.annotations.SerializedName;

public class BaseClass {

    protected long id;

    @SerializedName("first_name")
    protected String firstName;

    @SerializedName("last_name")
    protected String lastName;

    protected String gender;
    protected String dob;
    protected String email;
    protected String phone;
    protected String body;

    @SerializedName("_meta")
    protected Meta meta;
}
