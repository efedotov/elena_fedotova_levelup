package homework_8_rest_assured;

import com.google.gson.annotations.SerializedName;
import java.util.Objects;

public class Comment extends BaseClass {
    @SerializedName("post_id")
    protected int postId;
    private String name;


    public Comment(long id, int postId, String name, String email, String body) {
        this.id = id;
        this.postId = postId;
        this.name = name;
        this.email = email;
        this.body = body;
    }

    public Comment(int postId, String name, String email, String body) {
        this.postId = postId;
        this.name = name;
        this.email = email;
        this.body = body;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;
        Comment comment = (Comment) o;
        return getId() == comment.getId() &&
                getPostId() == comment.getPostId() &&
                Objects.equals(getName(), comment.getName()) &&
                Objects.equals(getEmail(), comment.getEmail()) &&
                Objects.equals(getBody(), comment.getBody());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPostId(), getName(), getEmail(), getBody());
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", postId=" + postId +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
