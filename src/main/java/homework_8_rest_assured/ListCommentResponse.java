package homework_8_rest_assured;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class ListCommentResponse extends BaseClass{
    @SerializedName("result")
    private List<Comment> result;

    public ListCommentResponse(Meta meta, List<Comment> result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public List<Comment> getResult() {
        return result;
    }

    public void setResult(List<Comment> result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListCommentResponse)) return false;
        ListCommentResponse that = (ListCommentResponse) o;
        return Objects.equals(getMeta(), that.getMeta()) &&
                Objects.equals(getResult(), that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeta(), getResult());
    }

    @Override
    public String toString() {
        return "ListCommentResponse{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}

