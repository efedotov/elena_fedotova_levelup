package homework_8_rest_assured;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class PostResponse extends BaseClass {

    @SerializedName("result")
    private Post result;

    public PostResponse(Meta meta, Post result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Post getResult() {
        return result;
    }

    public void setResult(Post result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PostResponse)) return false;
        PostResponse that = (PostResponse) o;
        return Objects.equals(getMeta(), that.getMeta()) &&
                Objects.equals(getResult(), that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeta(), getResult());
    }

    @Override
    public String toString() {
        return "PostResponse{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}

