package homework_8_rest_assured;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class UserResponse extends BaseClass {
    @SerializedName("result")
    private User result;

    public UserResponse(Meta meta, User result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public User getResult() {
        return result;
    }

    public void setResult(User result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserResponse)) return false;
        UserResponse that = (UserResponse) o;
        return Objects.equals(getMeta(), that.getMeta()) &&
                Objects.equals(getResult(), that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeta(), getResult());
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}

