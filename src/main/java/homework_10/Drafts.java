package homework_10;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.page;
import static org.openqa.selenium.By.xpath;

public class Drafts extends CommonClass {

    @FindBy(xpath ="//a[@data-title='Черновики']")
    private SelenideElement draft;

    @FindBy(className ="mail-MessageSnippet-Item_body")
    private SelenideElement openDraft;

    @FindBy(className ="ComposeControlPanelButton-Button_action")
    private SelenideElement send;

    @FindBy(xpath = "//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")
    private List<SelenideElement> listOfEmailsByTheme;

    @FindBy (className = "ComposeYabble-Text")
    private SelenideElement recipient;

    @FindBy (className = "mail-MessageSnippet-Item_subject")
    private SelenideElement themeEmail;

    @FindBy (className = "cke_contents_ltr")
    private SelenideElement emailBody;

    @FindBy (className = "b-messages__placeholder-item")
    private SelenideElement emptyDraft;

    @FindBy(className ="svgicon-mail--ComposeButton-Refresh")
    private SelenideElement refresh;

    public Drafts open() {
        return page(this);
    }

    @Step("Открыть папку \"Черновики\"")
    public void goToDraft () {
        draft.click();
    }

    @Step("Проверить, что письмо появилось в папке \"Черновики\"")
    public String readDraft () {
        draft.click();
        refresh.shouldBe(visible).click();
        $$(xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")).shouldHave(sizeGreaterThan(0));
        catchStaleExceptions(listOfEmailsByTheme);
        System.out.println(listOfEmailsByTheme.get(0).getText());
        return listOfEmailsByTheme.get(0).getText();
    }

    @Step("Открыть письмо")
    public void openDraft () {
        openDraft.click();
    }

    @Step("Проверить получателя")
    public String readRecipient () {
        return recipient.shouldBe(visible).getText();
    }

    @Step("Проверить тему письма")
    public String readThemeEmail () {
        return themeEmail.shouldBe(visible).getText();
    }

    @Step("Проверить содержимое письма")
    public String readEmailBody () {
        return emailBody.shouldBe(visible).getText();
    }

    @Step("Отправить письмо")
    public void sendEmail () {
        send.click();}

    @Step("Проверить, что письмо отправлено")
    public String emptiedDraft () {
        return emptyDraft.shouldBe(visible).getText();
    }
}

