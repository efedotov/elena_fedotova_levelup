package homework_10;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static com.codeborne.selenide.Condition.visible;

public class CommonClass {

    @FindBy(className = "js-toolbar-item-title-delete")
    protected SelenideElement delete;

    @FindBy(className = "mail-MessageSnippet-Checkbox-Nb")
    protected SelenideElement selectAll;

    @FindBy(className = "js-toolbar-item-delete")
    protected SelenideElement deleteSelected;

    @FindBy (className = "mail-Message-Toolbar-Subject_message")
    protected SelenideElement emailTheme;

    @FindBy (className = "mail-Message-Body-Content")
    protected SelenideElement emailBody;

    @FindBy(xpath ="//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")
    protected List<SelenideElement> listOfEmailsByTheme;


    public void catchStaleExceptions (List<SelenideElement> element) {
        try {
            element.get(0).shouldBe(visible).getText();
        } catch(StaleElementReferenceException e) {
            element.get(0).shouldBe(visible).getText();
        }
    }

    public void catchStaleExceptionsClick (SelenideElement element) {
        try {
            element.shouldBe(visible).click();
        } catch(StaleElementReferenceException e) {
            element.shouldBe(visible).click();
        }
    }

    public void catchStaleExceptionsWait (SelenideElement element) {
        try {
            element.shouldBe(visible);
        } catch(StaleElementReferenceException e) {
            element.shouldBe(visible);;
        }
    }

    public void deleteEmail () {
        delete.click();
        selectAll.click();
        deleteSelected.click();
    }
}
