package homework_10;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.page;
import static org.openqa.selenium.By.xpath;

public class Sent extends CommonClass {

    @FindBy(linkText = "Отправленные")
    private SelenideElement sentBox;

    @FindBy(xpath = "//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")
    private List<SelenideElement> listOfEmailsByTheme;

    @FindBy(className = "js-main-action-refresh")
    private SelenideElement refresh;

    @FindBy(className = "checkbox_view")
    private SelenideElement selectAll;

    @FindBy(className = "js-toolbar-item-title-delete")
    private SelenideElement delete;

    public Sent open() {
        return page(this);
    }

    @Step("Открыть папку \"Отправленные\"")
    public void goToSentBox() {
        sentBox.shouldBe(Condition.visible).click();
    }

    @Step("Проверить, что письмо появилось в папке \"Отправленные\"")
    public String refreshSendBox() {
        catchStaleExceptionsClick(refresh);
        $$(xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")).shouldHave(sizeGreaterThan(0));
        catchStaleExceptions(listOfEmailsByTheme);
//        listOfEmailsByTheme.get(0).shouldBe(Condition.visible);
        System.out.println(listOfEmailsByTheme.get(0).getText());
        return listOfEmailsByTheme.get(0).getText();
    }

    @Step("Очистить папку \"Отправленные\"")
    public void deleteEmail () {
        catchStaleExceptionsClick(selectAll);
        delete.click();
    }
}
