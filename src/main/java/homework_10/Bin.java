package homework_10;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;
import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.page;
import static org.openqa.selenium.By.xpath;

public class Bin extends CommonClass {

    @FindBy(xpath ="//a[@data-title='Удалённые']")

    private SelenideElement bin;
    public Bin open() {
        return page(this);
    }

    @Step("Открыть папку \"Удаленные\"")
    public void goToBin () {
        bin.shouldBe(Condition.visible).click();
    }

    @Step("Проверить, что письмо появилось в папке \"Удаленные\"")
    public String readBin () {
        $$(xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")).shouldHave(sizeGreaterThan(0));
        return listOfEmailsByTheme.get(0).getText();
    }
}
