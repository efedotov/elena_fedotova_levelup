package homework_7_selenide;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.page;

public class YandexMailInbox extends CommonClass {
    private static final String URL = "https://mail.yandex.ru";

    @FindBy (xpath ="//a[contains(@class,'HeadBanner-Button-Enter')]")
    private SelenideElement enterButton;

    @FindBy (id ="passp-field-login")
    private SelenideElement usernameTextField;

    @FindBy (className ="passp-sign-in-button")
    private SelenideElement signInButton;

    @FindBy (name ="passwd")
    private SelenideElement passwordTextField;

    @FindBy (className ="passp-sign-in-button")
    private SelenideElement passSinInButton;

    @FindBy (className ="svgicon-mail--ComposeButton")
    private SelenideElement composeButton;

    @FindBy (className ="composeYabbles")
    private SelenideElement addressInput;

    @FindBy (className ="ComposeSubject-TextField")
    private SelenideElement emailTheme;

    @FindBy (className ="cke_wysiwyg_div")
    private SelenideElement emailBody;

    @FindBy (className ="ComposeControlPanelButton-Button_icon")
    private SelenideElement save;

    @FindBy (className ="ComposeTimeOptions-Label")
    private SelenideElement timeSelection;

    @FindBy (xpath ="//div[@class='ComposePopup-Content']//button[contains(@class, 'controlButtons__btn--close')]")
    private SelenideElement closeEmail;

    public YandexMailInbox open() {
        Selenide.open(URL);
        return page(this);
    }

    public void login (String username, String password) {
        enterButton.shouldBe(visible).click();
        usernameTextField.sendKeys(username);
        signInButton.click();
        passwordTextField.sendKeys(password);
        passSinInButton.click();
    }

    public void writeEmail(String recipient, String theme, String content) {
        composeButton.shouldBe(visible).click();
        addressInput.click();
        addressInput.sendKeys(recipient);
        addressInput.sendKeys(Keys.ENTER);
        emailTheme.sendKeys(theme);
        emailBody.sendKeys(content);
    }

    public void saveAsDraft() {
        save.click();
        timeSelection.click();
        closeEmail.click();
    }
}
