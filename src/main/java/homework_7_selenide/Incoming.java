package homework_7_selenide;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.page;
import static org.openqa.selenium.By.xpath;

public class Incoming extends CommonClass {

    @FindBy(linkText ="Вернуться во \"Входящие\"")
    private SelenideElement incomingButton;

    @FindBy(className ="svgicon-mail--ComposeButton-Refresh")
    private SelenideElement refresh;

    @FindBy (xpath = "//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender']")
    private SelenideElement openEmail;

    @FindBy (xpath = "//div[@class='mail-MessageSnippet-Wrapper']/a[@class='mail-MessageSnippet js-message-snippet toggles-svgicon-on-important toggles-svgicon-on-unread']/div[@class='mail-MessageSnippet-Content']/span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_left js-message-snippet-left']/span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender']")
    private SelenideElement openEmailItem;

    @FindBy (className = "mail-Message-Sender-Email")
    private SelenideElement recipient;

    public Incoming open() {
        return page(this);
    }

    public void goToIncoming () {
        incomingButton.shouldBe(Condition.visible).click();
        refresh.shouldBe(Condition.visible).click();
    }

     public String readIncoming () {
        $$(xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")).shouldHave(sizeGreaterThan(0));
        return listOfEmailsByTheme.get(0).getText();
    }

    public void openEmail () {
        openEmail.shouldBe(Condition.visible).click();
        openEmailItem.shouldBe(Condition.visible).click();
    }

    public String readRecipient () {
        return recipient.shouldBe(Condition.visible).getText();
    }

    public String readThemeEmail () {
        return emailTheme.should(Condition.visible).getText();
    }

    public String readEmailBody () {
        return emailBody.shouldBe(Condition.visible).getText();
    }

    public void cleanUpIncomingBox () {
        deleteEmail();
    }

}

