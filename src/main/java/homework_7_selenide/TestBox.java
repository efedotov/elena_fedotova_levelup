package homework_7_selenide;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import static com.codeborne.selenide.Selenide.page;

public class TestBox extends CommonClass {

    @FindBy(xpath = "//a[@data-title='Test']")
    private SelenideElement testBox;

    @FindBy(xpath = "//span[contains(@class, 'js-message-snippet-firstline')]")
    private SelenideElement testEmail;

    @FindBy(xpath = "//div[@class='mail-MessageSnippet-Wrapper']/a[@class='mail-MessageSnippet js-message-snippet toggles-svgicon-on-important toggles-svgicon-on-unread']/div[@class='mail-MessageSnippet-Content']/span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_left js-message-snippet-left']/span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender']")
    private SelenideElement emailItem;

    @FindBy(className = "mail-Message-Sender-Email")
    private SelenideElement recipient;

    public TestBox open() {
        return page(this);
    }

    public void goToTestBox () {
        testBox.shouldBe(Condition.visible).click();
    }

    public void openEmailInTestBox () {
        catchStaleExceptionsWait(testEmail);
        testEmail.click();
        emailItem.shouldBe(Condition.visible).click();
    }

    public String readRecipient () {
        return recipient.shouldBe(Condition.visible).getText();
    }

    public String readThemeEmail () {
        return emailTheme.shouldBe(Condition.visible).getText();
    }

    public String readEmailBody () {
        return emailBody.shouldBe(Condition.visible).getText();
    }

    public void cleanUpTestBox () {
        deleteEmail();
    }
}
