package homework_2;

public abstract class  FlyingVehicle {
    public  String name;
    public  int capacity;
    public int carryingCapacity;
    public int rangeOfFlight;


    protected FlyingVehicle (String name, int capacity, int carryingCapacity, int rangeOfFlight) {
        this.name = name;
        this.capacity = capacity;
        this.carryingCapacity = carryingCapacity;
        this.rangeOfFlight = rangeOfFlight;
    }

    protected FlyingVehicle (String name, int rangeOfFlight) {
        this.name = name;
        this.rangeOfFlight = rangeOfFlight;
    }

    protected FlyingVehicle (String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getCapacity() { return capacity; }

    protected void setCapacity (int capacity) {
        this.capacity = capacity;
    }

    protected void setCarryingCapacity (int carryingCapacity)
    {this.carryingCapacity = carryingCapacity;
    }

    protected void setRangeOfFlight(int rangeOfFlight) {
        this.rangeOfFlight = rangeOfFlight;
    }

    }





