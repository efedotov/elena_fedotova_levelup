package homework_2.sorting;

import java.util.Objects;

public class VehicleComparable implements Comparable <VehicleComparable> {
    int rangeOfFlight;
    int capacity;
    String name;

    public VehicleComparable(String name, int rangeOfFlight) {
        this.name = name;
        this.rangeOfFlight = rangeOfFlight;
    }

    public VehicleComparable(String name, int capacity, int rangeOfFlight) {
        this.name = name;
        this.capacity = capacity;
        this.rangeOfFlight = rangeOfFlight;
    }

    public int getRangeOfFlight() {
        return rangeOfFlight;
    }

    public int getCapacity() {
        return capacity;
    }

    @Override
    public int compareTo (VehicleComparable o) {
        if (this.getRangeOfFlight() > o.getRangeOfFlight()) {
            return -1;
        }
        else if (this.getRangeOfFlight() == o.getRangeOfFlight()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public String toString() {
        if (capacity == 0) {return String.format ("%s, rangeOfFlight = %d \n", name, rangeOfFlight);
        } else
            return String.format("%s, rangeOfFlight = %d, capacity = %d \n", name, rangeOfFlight, capacity);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VehicleComparable that = (VehicleComparable) o;
        return rangeOfFlight == that.rangeOfFlight &&
                capacity == that.capacity &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rangeOfFlight, capacity, name);
    }
}






