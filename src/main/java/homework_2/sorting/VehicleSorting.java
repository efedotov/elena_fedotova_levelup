package homework_2.sorting;

import java.util.*;

public class VehicleSorting {
    public static void setTreeForVehicleComparable () {
        Set<VehicleComparable> vehicleSet = new TreeSet<>();
        vehicleSet.addAll(Arrays.asList(new VehicleComparable("Boeing767", 12000),
                new VehicleComparable("Boeing777", 8920), new VehicleComparable("AirbusA320", 5185),
                new VehicleComparable("AirbusA319", 4910), new VehicleComparable("Ka-62 Касатка", 720),
                new VehicleComparable("Аллигатор", 1110), new VehicleComparable("Phantom", 2),
                new VehicleComparable("Dron", 2), new VehicleComparable("Cloudhopper", 5),
                new VehicleComparable("Thunder", 7)));

        printVehicleComparable(vehicleSet);
    }

    public static void setTreeForVehicleWithComparator () {
        Set<VehicleComparable> vehicleSet1 = new TreeSet<>(new Comparator<VehicleComparable>() {
            @Override
            public int compare (VehicleComparable o1, VehicleComparable o2) {
                if (o1.getRangeOfFlight() == o2.getRangeOfFlight()) {
                    if (o1.getCapacity() > o2.getCapacity()) {
                        return 1;
                    } else if (o1.getCapacity() == o2.getCapacity()) {
                        return 1;
                    } else return -1;}
                else if (o1.getRangeOfFlight() > o2.getRangeOfFlight())
                    return 1;
                else return -1;
            }
        }
        );

        vehicleSet1.addAll(Arrays.asList(new VehicleComparable("Boeing767", 325, 12000),
                new VehicleComparable("Boeing777", 440,8920), new VehicleComparable("AirbusA320", 138,4910),
                new VehicleComparable("AirbusA319", 150, 4910), new VehicleComparable("Ka-62 Касатка", 15,720),
                new VehicleComparable("Аллигатор", 2,1110), new VehicleComparable("Phantom", 2),
                new VehicleComparable("Dron", 2), new VehicleComparable("Cloudhopper", 8, 5),
                new VehicleComparable("Thunder", 4,7)));

        printVehicleComparable(vehicleSet1);
    }

    static void printVehicleComparable (Set vehicleSet) {
        Iterator<VehicleComparable> iterator = vehicleSet.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next());
        }
    }
}
