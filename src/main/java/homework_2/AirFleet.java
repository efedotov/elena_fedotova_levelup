package homework_2;

import homework_2.sorting.VehicleSorting;
import homework_2.vehicles.Balloon;
import homework_2.vehicles.Helicopter;
import homework_2.vehicles.Plane;
import homework_2.vehicles.Quadcopter;

import java.io.*;
import java.util.*;

public class AirFleet {

    public static void main(String[] args) throws IOException {
        Plane boeing767 = new Plane("Boeing767", 325, 56000, 12000);
        Plane boeing777 = new Plane("Boeing777", 440, 57600, 8920);
        Plane airbusA319 = new Plane("AirbusA319", 150, 48500, 4910);
        Plane airbusA320 = new Plane("AirbusA320", 138, 52200, 5185);
        Helicopter kasatka = new Helicopter("Ka-62 Касатка", 15, 2200, 720);
        Helicopter alligator = new Helicopter("Аллигатор", 2, 4800, 1110);
        Quadcopter phantom = new Quadcopter("Phantom", 2);
        Quadcopter dron = new Quadcopter("Dron", 2);
        Balloon cloudhopper = new Balloon("Cloudhopper");
        Balloon thunder = new Balloon("Thunder ");

        cloudhopper.setRangeOfFlight(5);
        cloudhopper.setCarryingCapacity(480);
        cloudhopper.setCapacity(8);
        thunder.setRangeOfFlight(7);
        thunder.setCarryingCapacity(270);
        thunder.setCapacity(4);

        System.out.println("Парк компании 'AirFleet':");
        System.out.println("===============");
        List<FlyingVehicle> vehicleList = new ArrayList<>();
        vehicleList.add(boeing767);
        vehicleList.add(boeing777);
        vehicleList.add(airbusA319);
        vehicleList.add(airbusA320);
        vehicleList.add(kasatka);
        vehicleList.add(alligator);
        vehicleList.add(phantom);
        vehicleList.add(dron);
        vehicleList.add(cloudhopper);
        vehicleList.add(thunder);

        System.out.println(vehicleList);
        System.out.println();

        System.out.println("===============");
        countGeneralCapacity(vehicleList);
        countGeneralCarryingCapacity(vehicleList);
        System.out.println("===============");
        System.out.println();

        System.out.println("Сравнение транспортных средств по дальности полета в убывающем порядке:");
        VehicleSorting.setTreeForVehicleComparable();
        System.out.println();
        System.out.println("===============");

        System.out.println("Сравнение транспортных средств по дальности полета и количеству пассажиров в возрастающем порядке:");
        VehicleSorting.setTreeForVehicleWithComparator();
        System.out.println();
        System.out.println("===============");

        System.out.println("Вместительные самолеты с большой дальностью полета:");
        for (FlyingVehicle o : vehicleList) {
            if (o instanceof Plane && ((Plane) o).capacity >= 150 && ((Plane) o).rangeOfFlight > 5200) {
                    Plane p = (Plane) o;
                    System.out.println(p.getName());
            }
        }

        // Exceptions
        System.out.println();
        System.out.println("Unchecked exceptions:");
        AirFleet exception = new AirFleet();
        // ошибка в пути допущена специально
        File file = new File("C:\\Users\\efedotov\\Work\\Gigabit_TMI\\GIT\\Elena_Fedotova_LevelUP\\src\\listOfPlanes.txt");
        exception.readFromFile(file);
        vehicleList.add(null);
    }


    // Checked exceptions
    public void readFromFile(File file) throws IOException {
        FileReader fr = null;
        BufferedReader br = null;
        Set<String> result;
        try {
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            String line;
            result = new HashSet<>();
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        } catch (FileNotFoundException e) {
            System.err.println(file.getAbsolutePath() +
                    new MyExceptions("В парке нет самолетов!"));
        } catch (IOException e) {
            System.err.println(new MyExceptions ("Прочие возможные проблемы: ", e));
        } finally {
            if (fr != null) {
                fr.close();
            }
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    System.err.println(e);
                }
            }
        }
    }

    public static void countGeneralCapacity(List <FlyingVehicle> vehicleList)
    {
        int capacity = 0;
        for (FlyingVehicle vehicle: vehicleList) {
            capacity += vehicle.getCapacity(); // capacity = vehicle.getCapacity() + capacity;
        }
        System.out.println("General Capacity =\t" + capacity);
    }

    public static void countGeneralCarryingCapacity(List<FlyingVehicle> vehicleList)
    {
        int carryingCapacity = 0;
        for (FlyingVehicle vehicle: vehicleList) {
            carryingCapacity += vehicle.carryingCapacity;
        }
        System.out.println("General Carrying Capacity =\t" + carryingCapacity);
    }
}


































