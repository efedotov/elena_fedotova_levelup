package homework_2.vehicles;
import homework_2.FlyingVehicle;

public class Helicopter extends FlyingVehicle {
    public Helicopter(String name, int capacity, int carryingCapacity, int rangeOfFlight) {
        super(name, capacity, carryingCapacity, rangeOfFlight);
    }

    @Override
    public String toString() {
        return "Helicopter: " +
                "name='" + name + '\'' +
                ", capacity=" + capacity + '\'' + ", carryingCapacity=" + carryingCapacity + '\'' +", rangeOfFlight=" + rangeOfFlight + "\n";
    }
}
