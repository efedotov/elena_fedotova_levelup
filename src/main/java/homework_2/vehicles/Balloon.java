package homework_2.vehicles;
import homework_2.FlyingVehicle;

public class Balloon extends FlyingVehicle {
    public Balloon(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Balloon: " +
                "name='" + name + '\'' +
                ", capacity=" + capacity + '\'' + ", carryingCapacity=" + carryingCapacity + '\'' +", rangeOfFlight=" + rangeOfFlight +"\n";
    }
}
