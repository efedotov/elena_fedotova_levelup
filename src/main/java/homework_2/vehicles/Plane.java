package homework_2.vehicles;
import homework_2.FlyingVehicle;

public class Plane extends FlyingVehicle {
    public Plane(String name, int capacity, int carryingCapacity, int rangeOfFlight) {
        super(name, capacity, carryingCapacity, rangeOfFlight);
    }

    @Override
    public String toString() {
        return "Plane: " +
                "name='" + name + '\'' +
                ", capacity=" + capacity + '\'' + ", carryingCapacity=" + carryingCapacity + '\'' +", rangeOfFlight=" + rangeOfFlight + "\n";
    }

}
