package homework_2.vehicles;
import homework_2.FlyingVehicle;

public class Quadcopter extends FlyingVehicle {
    public Quadcopter(String name, int rangeOfFlight) {
        super (name, rangeOfFlight);
    }

    @Override
    public String toString() {
        return "Quadcopter: " +
                "name='" + name + '\'' + '\'' +", rangeOfFlight=" + rangeOfFlight + "\n";
    }
}
