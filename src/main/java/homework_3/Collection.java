package homework_3;

import java.util.*;

public class Collection {
    public static void main(String[] args) {
        Collection collection = new Collection();
        int i = 0;
        Integer[] arrayDigits = new Integer[100001];
//         создаем первоначальный массив arrayDigits
        while (i <= 100000) {
            arrayDigits[i] = i;
            i = ++i;
        }

//      System.out.println("Размер первоначального массива arrayDigits \t: " + arrayDigits.length);
//      collection.printArrayDigits(arrayDigits); // вывод первоначального массива

        collection.listOfDigits(arrayDigits); // добавляет элементы перемешанного массива в список digitList, операции со списком
    }

    void printArrayDigits(Integer[] arrayDigits) {
        for (int element : arrayDigits) {
            System.out.println(element);
        }
    }


    //     перемешивание массив
    void mixArray(Integer[] arrayDigits) {
        Random rnd = new Random();
        for (int i = 0; i <= arrayDigits.length - 1; i++) {
            int index = rnd.nextInt(i + 1);
            int a = arrayDigits[i]; // временная переменная, чтобы не потерять число с позицией [i]
            arrayDigits[i] = arrayDigits[index];
            arrayDigits[index] = a;
        }
    }

//         добавление массива в список, операции со списком
    public void listOfDigits (Integer[] arrayDigits) {
        mixArray(arrayDigits); // перемешиваем массив
//         System.out.println("Размер перемешанного массива:\t" + arrayDigits.length);
//         printArrayDigits(arrayDigits); // вывод перемешанного массива

//      помещаем перемешанный массив arrayDigitsMixed в digitList
        List<Integer> digitList = new ArrayList<>();
        digitList.addAll(Arrays.asList(arrayDigits));
//        System.out.println("Размер списка digitList: " + digitList.size());
//        printListOfDigits(digitList);

//      проверка на уникальность
        compareList(digitList);

//      сортировка по делителям
        division(digitList);
    }

    //     вывод списка
    void printListOfDigits(List<Integer> digitList) {
        Iterator<Integer> iterator = digitList.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
    }

    //     сравнение элементов списка
    public void compareList(List<Integer> digitList) {
        int i = 0;
        int j;
        do {
            j = i + 1;
            while (digitList.get(i) != digitList.get(j) && j < digitList.size() - 1)
                j++;
            i++;
        } while (i < digitList.size() - 1 && j == digitList.size());
        if (j < digitList.size() - 1)
            System.out.println("В списке есть повторяющиеся элементы");
        else System.out.println("Все элементы в списке уникальны");
    }

    public void division (List<Integer> digitList) {
        TreeSet<Integer> digitSetDiv2 = new TreeSet<>();
        TreeSet<Integer> digitListDiv3 = new TreeSet<>();
        TreeSet<Integer> digitListDiv5 = new TreeSet<>();
        TreeSet<Integer> digitListDiv7 = new TreeSet<>();

        Map<Integer, TreeSet<Integer>> divMap = new HashMap<>();

        for (int element : digitList) {
            if (digitList.get(element) % 2 == 0) {
                digitSetDiv2.add(digitList.get(element));
                divMap.put(2, digitSetDiv2);
            }
            else if (digitList.get(element) % 3 == 0) {
                digitListDiv3.add(digitList.get(element));
                divMap.put(3, digitListDiv3);
            }
            else if (digitList.get(element) % 5 == 0) {
                digitListDiv5.add(digitList.get(element));
                divMap.put(5, digitListDiv5);
            }
            else if (digitList.get(element) % 7 == 0) {
                digitListDiv7.add(digitList.get(element));
                divMap.put(7, digitListDiv7);
            }
        }

        printMapOfDigits(divMap);
    }

    public void printMapOfDigits(Map<Integer, TreeSet<Integer>> digitMap) {
        System.out.println("Run through the map by entry table");
        Set<Map.Entry<Integer, TreeSet<Integer>>> entries = digitMap.entrySet();
        for (Map.Entry<Integer,TreeSet<Integer>> entry : entries) {
            System.out.println(String.format("На '%s' без остатка делятся '%s' \n", entry.getKey(), entry.getValue()));
        }
    }
}


