package homework_1;

import java.util.Scanner;

public class Input {
    public static double inputDouble() {
        System.out.print("Введите число: ");
        Scanner scan1 = new Scanner(System.in);
        double digit1 = scan1.nextDouble();
        return digit1;
    }
    public static int inputInt() {
        System.out.print("Введите число: ");
        Scanner scan2 = new Scanner(System.in);
        int digit2 = scan2.nextInt();
        return digit2;
    }
}
