package homework_1;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Calculator app = new Calculator();
        app.startCalculator();
    }

    public void startCalculator() {
        Addition add = new Addition();
        Exponentiation exp = new Exponentiation();
        Factorial fact = new Factorial();
        Fibonacci fibon = new Fibonacci();
        Multiplication mult = new Multiplication();
        Subtraction sub = new Subtraction();

        int n = 0;
        while (n == 0) {
            System.out.println("Введите операцию с числами. Допустимые значения:");
            System.out.println("Сложение: 1");
            System.out.println("Вычитание: 2");
            System.out.println("Умножение: 3");
            System.out.println("Возведение в степень: 4");
            System.out.println("Факториал: 5");
            System.out.println("Вычисление числа Фибоначчи: 6");
            Scanner scanner = new Scanner(System.in);
            int operation = scanner.nextInt();

            switch (operation) {
                case 1:
                    add.addition();
                    break;
                case 2:
                    sub.subtraction();
                    break;
                case 3:
                    mult.multiplication();
                    break;
                case 4:
                    exp.exponentiation();
                    break;
                case 5:
                    fact.factorial();
                    break;
                case 6:
                    fibon.fibonacci();
            }
            System.out.println();
            System.out.println("Чтобы продолжить вычисления, введите 0");
            System.out.println("Чтобы выйти из программы, введите любое другое число");
            Scanner scan4 = new Scanner(System.in);
            n = scan4.nextInt();
        }
    }
}

