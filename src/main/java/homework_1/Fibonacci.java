package homework_1;

public class Fibonacci {
    public void fibonacci () {
        int digit1 = Input.inputInt();
        int fib1 = 1;
        int fib2 = 1;
        int fib =1;
        int i = 1;

        while (i<digit1-1) {
            fib = fib1 + fib2;
            fib1 = fib2;
            fib2 = fib;
            i=i+1;
        }
        System.out.print("Число Фибоначчи под номером " + digit1 + " = " + fib);
    }
}
