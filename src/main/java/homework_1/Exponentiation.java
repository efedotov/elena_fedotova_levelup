package homework_1;

public class Exponentiation {
    public void exponentiation () {
        int digit1 = Input.inputInt();
        int digit2 = Input.inputInt();
        int i=1;
        int exp = 0;
        int n = digit1;

        while (i<digit2) {
            exp = digit1*n;
            n = exp;
            i=++i;
        }
        System.out.print( digit1 + " в степени " + digit2 + " = " + exp);
    }
}
