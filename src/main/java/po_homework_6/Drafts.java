package po_homework_6;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;


public class Drafts extends CommonClass {

    @FindBy(xpath ="//a[@data-title='Черновики']")
    private WebElement draft;

    @FindBy(className ="mail-MessageSnippet-Item_body")
    private WebElement openDraft;

    @FindBy(className ="ComposeControlPanelButton-Button_action")
    private WebElement send;

    @FindBy(xpath = "//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")
    private List<WebElement> listOfEmailsByTheme;

    @FindBy (className = "ComposeYabble-Text")
    private WebElement recipient;

    @FindBy (className = "mail-MessageSnippet-Item_subject")
    private WebElement themeEmail;

    @FindBy (className = "cke_contents_ltr")
    private WebElement emailBody;

    @FindBy (className = "b-messages__placeholder-item")
    private WebElement emptyDraft;

    public Drafts (WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public void goToDraft () {
        wait.until(elementToBeClickable(draft)).click();
    }

    public String readDraft () {
        wait.until(elementToBeClickable(draft)).click();
        wait.until(visibilityOfAllElements(listOfEmailsByTheme));
        System.out.println(listOfEmailsByTheme.get(0).getText());
        return listOfEmailsByTheme.get(0).getText();
    }

    public void openDraft () {
        wait.until(elementToBeClickable(openDraft)).click();
    }

    public String readRecipient () {
        wait.until(visibilityOf(recipient));
        return recipient.getText();
    }

    public String readThemeEmail () {
        wait.until(visibilityOf(themeEmail));
        return themeEmail.getText();
    }

    public String readEmailBody () {
        wait.until(visibilityOf(emailBody));
        return emailBody.getText();
    }

    public void sendEmail () {
        wait.until(elementToBeClickable(send)).click();}

    public String emptiedDraft () {
        wait.until(visibilityOf(emptyDraft));
    return emptyDraft.getText();
    }
}

