package po_homework_6;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfAllElements;

public class Bin extends CommonClass{

    @FindBy(xpath ="//a[@data-title='Удалённые']")
    private WebElement bin;

    @FindBy(xpath ="//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")
    private List<WebElement> listOfEmailsByTheme;

    public Bin (WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public void goToBin () {
        wait.until(elementToBeClickable(bin)).click();
    }

    public String readBin () {
        wait.until(visibilityOfAllElements(listOfEmailsByTheme));
        return listOfEmailsByTheme.get(0).getText();
    }
}
