package po_homework_6;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class Incoming extends CommonClass {

    @FindBy(linkText ="Вернуться во \"Входящие\"")
    private WebElement incomingButton;

    @FindBy(className ="svgicon-mail--ComposeButton-Refresh")
    private WebElement refresh;

    @FindBy(xpath ="//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")
    private List<WebElement> listOfEmailsByTheme;

    @FindBy (xpath = "//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender']")
    private WebElement openEmail;

    @FindBy (xpath = "//div[@class='mail-MessageSnippet-Wrapper']/a[@class='mail-MessageSnippet js-message-snippet toggles-svgicon-on-important toggles-svgicon-on-unread']/div[@class='mail-MessageSnippet-Content']/span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_left js-message-snippet-left']/span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender']")
    private WebElement openEmailItem;

    @FindBy (className = "mail-Message-Sender-Email")
    private WebElement recipient;

    public Incoming (WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public void goToIncoming () {
        wait.until(elementToBeClickable(incomingButton)).click();
        wait.until(elementToBeClickable(refresh)).click();
    }

    public String readIncoming () {
        wait.until(visibilityOfAllElements(listOfEmailsByTheme));
        return listOfEmailsByTheme.get(0).getText();
    }

    public void openEmail () {
        wait.until(elementToBeClickable(openEmail)).click();
        wait.until(elementToBeClickable(openEmailItem)).click();
    }

    public String readRecipient () {
        wait.until(visibilityOf(recipient));
        return recipient.getText();
    }

    public String readThemeEmail () {
        wait.until(visibilityOf(emailTheme));
        return emailTheme.getText();
    }

    public String readEmailBody () {
        wait.until(visibilityOf(emailBody));
        return emailBody.getText();
    }

    public void cleanUpIncomingBox () {
        deleteEmail();
    }

}
