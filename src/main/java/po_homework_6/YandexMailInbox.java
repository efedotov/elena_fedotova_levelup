package po_homework_6;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class YandexMailInbox extends CommonClass {
    private static final String URL = "https://mail.yandex.ru";

    @FindBy (xpath ="//a[contains(@class,'HeadBanner-Button-Enter')]")
    private WebElement enterButton;

    @FindBy (id ="passp-field-login")
    private WebElement usernameTextField;

    @FindBy (className ="passp-sign-in-button")
    private WebElement signInButton;

    @FindBy (name ="passwd")
    private WebElement passwordTextField;

    @FindBy (className ="passp-sign-in-button")
    private WebElement passSinInButton;

    @FindBy (className ="svgicon-mail--ComposeButton")
    private WebElement composeButton;

    @FindBy (className ="composeYabbles")
    private WebElement addressInput;

    @FindBy (className ="ComposeSubject-TextField")
    private WebElement emailTheme;

    @FindBy (className ="cke_wysiwyg_div")
    private WebElement emailBody;

    @FindBy (className ="ComposeControlPanelButton-Button_icon")
    private WebElement save;

    @FindBy (className ="ComposeTimeOptions-Label")
    private WebElement timeSelection;

    @FindBy (xpath ="//div[@class='ComposePopup-Content']//button[contains(@class, 'controlButtons__btn--close')]")
    private WebElement closeEmail;

    public YandexMailInbox  (WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public void open() {
        driver.get(URL);
    }

    public void login (String username, String password) {
        catchStaleExceptionsClick(enterButton, elementToBeClickable(enterButton));
        wait.until(visibilityOf(usernameTextField)).sendKeys(username);
        wait.until(elementToBeClickable(signInButton)).click();
        wait.until(visibilityOf(passwordTextField)).sendKeys(password);
        wait.until(ExpectedConditions.elementToBeClickable(passSinInButton)).click();
    }

    public void writeEmail(String recipient, String theme, String content) {
        wait.until(elementToBeClickable(composeButton)).click();
        wait.until(visibilityOf(addressInput));
        Actions actions = new Actions(driver);
        actions
                .click(addressInput)
                .sendKeys(recipient)
                .sendKeys(Keys.ENTER)
                .build()
                .perform();
        wait.until(ExpectedConditions.visibilityOf(emailTheme)).sendKeys(theme);
        wait.until(visibilityOf(emailBody)).sendKeys(content);
    }

    public void saveAsDraft() {
        wait.until(elementToBeClickable(save)).click();
        wait.until(elementToBeClickable(timeSelection)).click();
        wait.until(elementToBeClickable(closeEmail)).click();

    }
}
