package po_homework_6;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class TestBox extends CommonClass {

    @FindBy(xpath = "//a[@data-title='Test']")
    private WebElement testBox;

    @FindBy(xpath = "//span[contains(@class, 'js-message-snippet-firstline')]")
    private WebElement testEmail;

    @FindBy(xpath = "//div[@class='mail-MessageSnippet-Wrapper']/a[@class='mail-MessageSnippet js-message-snippet toggles-svgicon-on-important toggles-svgicon-on-unread']/div[@class='mail-MessageSnippet-Content']/span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_left js-message-snippet-left']/span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender']")
    private WebElement emailItem;

    @FindBy(className = "mail-Message-Sender-Email")
    private WebElement recipient;

    public TestBox(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public void goToTestBox () {
        wait.until(elementToBeClickable(testBox)).click();
    }

    public void openEmailInTestBox () {
        catchStaleExceptions(elementToBeClickable(testEmail));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", testEmail);
         wait.until(elementToBeClickable(emailItem)).click();
    }

    public String readRecipient () {
        wait.until(visibilityOf(recipient));
        return recipient.getText();
    }

    public String readThemeEmail () {
        wait.until(visibilityOf(emailTheme));
        return emailTheme.getText();
    }

    public String readEmailBody () {
        wait.until(visibilityOf(emailBody));
        return emailBody.getText();
    }

    public void cleanUpTestBox () {
        deleteEmail();
    }
}
