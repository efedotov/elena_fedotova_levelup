package po_homework_6;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class CommonClass {

    protected WebDriver driver;
    protected WebDriverWait wait;

    @FindBy(className = "js-toolbar-item-title-delete")
    protected WebElement delete;

    @FindBy(className = "mail-MessageSnippet-Checkbox-Nb")
    protected WebElement selectAll;

    @FindBy(className = "js-toolbar-item-delete")
    protected WebElement deleteSelected;

    @FindBy (className = "mail-Message-Toolbar-Subject_message")
    protected WebElement emailTheme;

    @FindBy (className = "mail-Message-Body-Content")
    protected WebElement emailBody;

    public CommonClass  (WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public void catchStaleExceptionsClick (WebElement element, ExpectedCondition condition) {
        try {
           wait.until(condition);
            element.click();
        } catch(StaleElementReferenceException e) {
           wait.until(condition);
          element.click();
        }
    }

    public void catchStaleExceptions (ExpectedCondition condition) {
        try {
            wait.until(condition);
                   } catch(StaleElementReferenceException e) {
            wait.until(condition);

        }
    }

    public void deleteEmail () {
        wait.until(elementToBeClickable(delete)).click();
        wait.until(elementToBeClickable(selectAll)).click();
        wait.until(elementToBeClickable(deleteSelected)).click();
    }
}
