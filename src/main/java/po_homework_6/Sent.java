package po_homework_6;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfAllElements;

public class Sent extends CommonClass {

    @FindBy(linkText = "Отправленные")
    private WebElement sentBox;

    @FindBy(xpath = "//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']")
    private List<WebElement> listOfEmailsByTheme;

    @FindBy(className = "js-main-action-refresh")
    private WebElement refresh;

    @FindBy(className = "checkbox_view")
    private WebElement selectAll;

    @FindBy(className = "js-toolbar-item-title-delete")
    private WebElement delete;

    public Sent(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public void goToSentBox() {
        wait.until(elementToBeClickable(sentBox)).click();
    }

    public String refreshSendBox() {
        catchStaleExceptionsClick(refresh, elementToBeClickable(refresh));
        catchStaleExceptions(visibilityOfAllElements(listOfEmailsByTheme));
        System.out.println(listOfEmailsByTheme.get(0).getText());
        return listOfEmailsByTheme.get(0).getText();
    }

    public void deleteEmail () {
        catchStaleExceptionsClick(selectAll,(elementToBeClickable(selectAll)));
        wait.until(elementToBeClickable(delete)).click();
    }
}
